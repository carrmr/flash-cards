# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130504170448) do

  create_table "categories", :force => true do |t|
    t.string "name", :null => false
  end

  create_table "exams", :force => true do |t|
    t.string  "name",        :null => false
    t.text    "description"
    t.integer "category_id", :null => false
  end

  create_table "exams_terms", :id => false, :force => true do |t|
    t.integer "exam_id"
    t.integer "term_id"
  end

  create_table "terms", :force => true do |t|
    t.string "word",       :null => false
    t.text   "definition", :null => false
  end

end
