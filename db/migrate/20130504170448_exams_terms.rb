class ExamsTerms < ActiveRecord::Migration
  def self.up
    create_table :exams_terms, :id=>false do |t|
	t.column :exam_id, :integer
	t.column :term_id, :integer
    end
  end

  def self.down
    drop_table :exams_terms
  end
end
