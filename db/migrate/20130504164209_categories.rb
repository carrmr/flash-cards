class Categories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
	t.column :name, :string, :null=>false, :unique=>true
    end
  end

  def self.down
    drop_table :categories
  end
end
