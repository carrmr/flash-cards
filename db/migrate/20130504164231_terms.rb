class Terms < ActiveRecord::Migration
  def self.up
    create_table :terms do |t|
	t.column :word, :string, :null=>false, :unique=>true
	t.column :definition, :text, :null=>false
    end
  end

  def self.down
    drop_table :terms
  end
end
