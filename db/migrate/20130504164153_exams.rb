class Exams < ActiveRecord::Migration
  def self.up
    create_table :exams do |t|
	t.column :name, :string, :null=>false, :unique=>true
	t.column :description, :text
	t.column :category_id, :integer, :null=>false
    end
  end

  def self.down
    drop_table :exams
  end
end
