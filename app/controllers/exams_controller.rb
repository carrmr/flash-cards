class ExamsController < ApplicationController
  def index
    @exams = Exam.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @exams }
    end
  end

  def show
    @exam = Exam.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @exam }
    end
  end

  def new
    @exam = Exam.new
    @categories = Category.all
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @exam }
    end
  end

  def edit
    @exam = Exam.find(params[:id])
    @categories = Category.all
  end

  def create
    @exam = Exam.new(params[:exam])

    respond_to do |format|
      if @exam.save
        format.html { redirect_to @exam, :notice => 'Exam was successfully created.' }
        format.json { render :json => @exam, :status => :created, :location => @exam }
      else
        format.html { render :action => "new" }
        format.json { render :json => @exam.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @exam = Exam.find(params[:id])

    respond_to do |format|
      if @exam.update_attributes(params[:exam])
        format.html { redirect_to @exam, :notice => 'Exam was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @exam.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @exam = Exam.find(params[:id])
    @exam.destroy

    respond_to do |format|
      format.html { redirect_to exams_url }
      format.json { head :no_content }
    end
  end
end
