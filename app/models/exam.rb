class Exam < ActiveRecord::Base
  attr_accessible :name, :description, :category_id, :term_ids
  belongs_to :category
  has_and_belongs_to_many :terms
  validates_uniqueness_of :name
  validates_presence_of :name
end
