class Term < ActiveRecord::Base
  attr_accessible :word, :definition, :exam_ids
  has_and_belongs_to_many :exams
  validates_uniqueness_of :word
  validates_presence_of :word
  validates_presence_of :definition
end
