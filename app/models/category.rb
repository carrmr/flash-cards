class Category < ActiveRecord::Base
  attr_accessible :name
  has_many :exams
  validates_uniqueness_of :name
  validates_presence_of :name
end
